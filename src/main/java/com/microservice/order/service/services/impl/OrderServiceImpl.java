package com.microservice.order.service.services.impl;

import com.microservice.order.service.entity.Order;
import com.microservice.order.service.exception.NotFoundException;
import com.microservice.order.service.repository.OrderRepository;
import com.microservice.order.service.request.CreateOrderRequest;
import com.microservice.order.service.services.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class OrderServiceImpl implements OrderService {

    @Autowired OrderRepository orderRepository;

    @Override
    public Order createOrder(CreateOrderRequest createOrderRequest) {
        Order order = new Order();
        order.setName(createOrderRequest.getName());
        order.setQty(createOrderRequest.getQty());
        order.setPrice(createOrderRequest.getPrice());
        order.setCreatedAt(new Date());
        order.setUpdatedAt(new Date());

        orderRepository.save(order);

        return order;
    }

    @Override
    public Order getOrderById(String id) {
        Order order =findOrderByIdOrThrowNotFound(id);

        return order;
    }

    @Override
    public List<Order> getAllOrders() {
        return orderRepository.findAll();
    }

    private Order findOrderByIdOrThrowNotFound(String id) {
        Order order = orderRepository.findById(id).orElse(null);

        if (order == null) {
            throw new NotFoundException();
        } else {
            return order;
        }
    }
}
