package com.microservice.order.service.controller;

import com.microservice.order.service.common.PaymentRequest;
import com.microservice.order.service.common.PaymentServiceProxy;
import com.microservice.order.service.entity.Order;
import com.microservice.order.service.request.CreateOrderRequest;
import com.microservice.order.service.response.ApiResponse;
import com.microservice.order.service.services.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api")
public class OrderController {

    @Autowired private OrderService orderService;
    @Autowired private PaymentServiceProxy paymentServiceProxy;

    @PostMapping(
            value = {"/order"},
            produces = {"application/json"},
            consumes = {"application/json"}
    )
    public ResponseEntity<ApiResponse> createOrder(@Valid @RequestBody CreateOrderRequest request) {
        Order order = orderService.createOrder(request);
        PaymentRequest paymentRequest = new PaymentRequest(UUID.randomUUID().toString(), order.getId(), order.getPrice() * order.getQty());
        ApiResponse<Order> apiResponse = new ApiResponse<Order>(201, true, order);
        paymentServiceProxy.createOrder(paymentRequest);
        return new ResponseEntity<ApiResponse>(apiResponse, HttpStatus.CREATED);
    }

    @GetMapping(
            value = {"/order/{id}"},
            produces = {"application/json"}
    )
    public ResponseEntity<ApiResponse> getOrderById(@PathVariable("id") String id) {
        Order order = orderService.getOrderById(id);
        ApiResponse<Order> apiResponse = new ApiResponse<Order>(200, true, order);

        return new ResponseEntity<ApiResponse>(apiResponse, HttpStatus.OK);
    }

    @GetMapping(
            value = {"/order"},
            produces = {"application/json"}
    )
    public ResponseEntity<ApiResponse> getAllOrders() {
        List<Order> orders = orderService.getAllOrders();
        ApiResponse<Object> apiResponse = new ApiResponse<Object>(200, true, orders);

        return new ResponseEntity<ApiResponse>(apiResponse, HttpStatus.OK);
    }
}
