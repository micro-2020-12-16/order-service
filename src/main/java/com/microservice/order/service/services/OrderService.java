package com.microservice.order.service.services;

import com.microservice.order.service.entity.Order;
import com.microservice.order.service.request.CreateOrderRequest;

import java.util.List;

public interface OrderService {

    public Order createOrder(CreateOrderRequest createOrderRequest);

    public Order getOrderById(String id);

    public List<Order> getAllOrders();
}
