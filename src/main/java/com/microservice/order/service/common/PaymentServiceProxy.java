package com.microservice.order.service.common;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;

@FeignClient(name = "cloud-gateway")
public interface PaymentServiceProxy {

    @PostMapping("/payment-service/api/payment")
    public void createOrder(@Valid @RequestBody PaymentRequest paymentRequest);
}
